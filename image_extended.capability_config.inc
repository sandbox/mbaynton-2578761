<?php

use Drupal\Core\Render\Element;

/**
 * @param array $variables
 *   An associative array containing:
 *   - element: A render element representing the capability configuration form.
 */
function template_preprocess_image_extended_capability_config(&$variables) {
  $element = $variables['element'];

  $variables['data'] = array();
  foreach (Element::children($element) as $child) {
    $variables['data'][$child] = $element[$child];
  }

}
