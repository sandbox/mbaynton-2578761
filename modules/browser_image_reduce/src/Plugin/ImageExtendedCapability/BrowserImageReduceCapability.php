<?php
namespace Drupal\browser_image_reduce\Plugin\ImageExtendedCapability;

use Drupal\image_extended\ImageExtendedCapabilityBase;

/**
 * Image Extended Capability plugin for reducing image size as needed 
 * via HTML5 canvas before uploading.
 * 
 * @ImageExtendedCapability(
 *   id = "browser_image_reduce_capability",
 *   label = @Translation("Browser Image Reduce")
 * )
 */
class BrowserImageReduceCapability extends ImageExtendedCapabilityBase {
    
}
