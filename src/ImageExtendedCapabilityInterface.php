<?php

namespace Drupal\image_extended;

use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin interface for Image Extended capabilities. 
 * 
 * Image Extended capabilities can extend or alter the Image Extended 
 * field type, widget, and/or formatter.
 */
interface ImageExtendedCapabilityInterface {
  /* Section: Methods similar to those in Drupal\Core\Field\FieldItemInterface
   * These impact the Image Extended field type.
   */

  /**
   * Defines the field item properties used by the extended capability.
   * Properties an ImageExtendedCapability plugin defines are reported
   * to core in addition to those defined by the core image field and
   * those defined by other enabled ImageExtendedCapability plugins.
   * 
   * @return \Drupal\Core\TypedData\DataDefinitionInterface[]
   *   An array of property definitions of contained properties, keyed by
   *   property name.
   *
   * @see \Drupal\Core\Field\BaseFieldDefinition
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition);
  
  /**
   * Returns the schema for the item properties used by the extended capability.
   *
   * The array format of the returned value is identical to that returned by
   * Drupal\Core\Field\FieldItemInterface::schema. Columns, indexes, and all
   * other keys are simply recursively merged into the final schema for the
   * Image Extended field.
   *
   * Computed fields having no schema should return an empty array.
   * 
   * @param \Drupal\Core\Field\FieldStorageDefinitionInterface $field_definition
   *   The field definition.
   *
   * @return array
   *   An empty array if there is no schema, or an associative array with the
   *   following key/value pairs:
   *   - columns: An array of Schema API column specifications, keyed by column
   *     name. The columns need to be a subset of the properties defined in
   *     propertyDefinitions(). The 'not null' property is ignored if present,
   *     as it is determined automatically by the storage controller depending
   *     on the table layout and the property definitions. It is recommended to
   *     avoid having the column definitions depend on field settings when
   *     possible. No assumptions should be made on how storage engines
   *     internally use the original column name to structure their storage.
   *   - unique keys: (optional) An array of Schema API unique key definitions.
   *   - indexes: (optional) An array of Schema API index definitions.
   *   - foreign keys: (optional) An array of Schema API foreign key
   *     definitions. Note, however, that the field data is not necessarily
   *     stored in SQL. Also, the possible usage is limited, as you cannot
   *     specify another field as related, only existing SQL tables,
   *     such as {taxonomy_term_data}.
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition);
  
  /**
   * Defines custom presave behavior for field values.
   *
   * This method is called during the process of saving an entity, just before
   * values are written into storage. When storing a new entity, its identifier
   * will not be available yet. This should be used to massage item property
   * values or perform any other operation that needs to happen before values
   * are stored. For instance this is the proper phase to auto-create a new
   * entity for an entity reference field item, because this way it will be
   * possible to store the referenced entity identifier.
   * 
   * @param \Drupal\Core\Field\FieldItemInterface 
   *    The entire image field.
   */
  public function preSave(FieldItemInterface $entireFieldItem);
  
  /**
   * Defines custom post-save behavior for field values.
   *
   * This method is called during the process of saving an entity, just after
   * values are written into storage. This is useful mostly when the business
   * logic to be implemented always requires the entity identifier, even when
   * storing a new entity. For instance, when implementing circular entity
   * references, the referenced entity will be created on pre-save with a dummy
   * value for the referring entity identifier, which will be updated with the
   * actual one on post-save.
   *
   * In the rare cases where item properties depend on the entity identifier,
   * massaging logic will have to be implemented on post-save and returning TRUE
   * will allow them to be rewritten to the storage with the updated values.
   *
   * @param \Drupal\Core\Field\FieldItemInterface 
   *    The entire image field.
   * 
   * @param bool $update
   *   Specifies whether the entity is being updated or created.
   *
   * @return bool
   *   Whether field items should be rewritten to the storage as a consequence
   *   of the logic implemented by the custom behavior.
   */
  public function postSave(FieldItemInterface $entireFieldItem, $update);
  
  /**
   * Defines custom delete behavior for field values.
   *
   * This method is called during the process of deleting an entity, just before
   * values are deleted from storage.
   * 
   * @param \Drupal\Core\Field\FieldItemInterface 
   *    The entire image field.
   */
  public function delete(FieldItemInterface $entireFieldItem);

  /**
   * Defines custom revision delete behavior for field values.
   *
   * This method is called from during the process of deleting an entity
   * revision, just before the field values are deleted from storage. It is only
   * called for entity types that support revisioning.
   * 
   * @param \Drupal\Core\Field\FieldItemInterface 
   *    The entire image field.
   */  
  public function deleteRevision(FieldItemInterface $entireFieldItem);
  
  /**
   * Defines the field-level settings for this capability.
   *
   * @param \Drupal\Core\Field\FieldItemInterface 
   *   The entire (but possibly unfinished) image field.
   *
   * @return array
   *   A list of default settings, keyed by the setting name.
   */
  public static function defaultFieldSettings();
  
  /**
   * Returns a renderable array of form elements for this capabilities 
   * field-level settings.
   *
   * @param \Drupal\Core\Field\FieldItemInterface 
   *    The entire (but probably unfinished) image field.
   * @param array $form
   *   The form where the settings elements are being included in.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state of the (entire) configuration form.
   *
   * @return array
   *   The form definition for the field settings.
   */
  public function fieldSettingsForm(FieldItemInterface $entireFieldItem, array $form, FormStateInterface $form_state);
  
  
  /*
   * Section: 
   */

}
