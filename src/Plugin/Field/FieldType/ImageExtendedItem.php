<?php

/**
 * @image
 * Contains \Drupal\image_extended\Plugin\Field\FieldType\ImageExtendedItem.
 */

namespace Drupal\image_extended\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\image\Plugin\Field\FieldType\ImageItem;
use Drupal\image_extended\ImageExtendedCapabilityTypeUserTrait;

/**
 * Plugin implementation of the 'image' field type.
 *
 * @FieldType(
 *   id = "image_extended",
 *   label = @Translation("Image Extended"),
 *   description = @Translation("This field stores the ID of an image file as an integer value."),
 *   category = @Translation("Reference"),
 *   default_widget = "image_extended_image_extended",
 *   default_formatter = "image",
 *   column_groups = {
 *     "file" = {
 *       "label" = @Translation("File"),
 *       "columns" = {
 *         "target_id", "width", "height"
 *       }
 *     },
 *     "alt" = {
 *       "label" = @Translation("Alt"),
 *       "translatable" = TRUE
 *     },
 *     "title" = {
 *       "label" = @Translation("Title"),
 *       "translatable" = TRUE
 *     },
 *   },
 *   list_class = "\Drupal\file\Plugin\Field\FieldType\FileFieldItemList",
 *   constraints = {"ValidReference" = {}, "ReferenceAccess" = {}}
 * )
 */
class ImageExtendedItem extends ImageItem {
  use ImageExtendedCapabilityTypeUserTrait;
  
  // TODO: make schema dependent on the enabled capabilities; waiting for core
  // to be more stable with the dynamic schema updates ->
  // https://www.drupal.org/node/2532864
  
  protected static function pluginManager() {
      return \Drupal::service('plugin.manager.image_extended.capability');
  }
  
  /**
   * {@inheritdoc}
   */
  public static function defaultFieldSettings() {
      return self::gatherDefaultFieldSettings();
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return self::gatherSchema($field_definition);
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
      return self::gatherPropertyDefinitions($field_definition);
  }
  
  /**
   * {@inheritdoc}
   */
  public function fieldSettingsForm(array $form, FormStateInterface $form_state) {
      return $this->gatherFieldSettingsForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */  
  public function preSave() {
      $this->invokeCapabilitiesPresave();
  }
  
  /**
   * {@inheritdoc}
   */
  public function postSave($update) {
      return $this->invokeCapabilitiesPostSave($update);
  }
  
  /**
   * {@inheritdoc}
   */
  public function delete() {
      $this->invokeCapabilitiesDelete();
  }

  /**
   * {@inheritdoc}
   */
  public function deleteRevision() {
      $this->invokeCapabilitiesDeleteRevision();
  }
}
