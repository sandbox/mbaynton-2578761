<?php

/**
 * @file
 * Contains \Drupal\image_extended\Plugin\Field\FieldWidget\ImageExtendedWidget.
 */

namespace Drupal\image_extended\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Form\FormStateInterface;
use Drupal\image\Plugin\Field\FieldWidget\ImageWidget;

/**
 * Plugin implementation of the 'image_extended_image_extended' widget.
 *
 * @FieldWidget(
 *   id = "image_extended_image_extended",
 *   label = @Translation("Image Extended"),
 *   field_types = {
 *     "image_extended"
 *   }
 * )
 */
class ImageExtendedWidget extends ImageWidget {
    public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
        $element = parent::formElement($items, $delta, $element, $form, $form_state);
        
        $element['teststuff'] = array(
            '#markup' => 'boo!',
        );
        
        return $element;
    }
}