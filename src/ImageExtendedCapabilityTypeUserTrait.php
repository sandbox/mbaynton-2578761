<?php
namespace Drupal\image_extended;

use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Zend\Stdlib\ArrayUtils;

/**
 * A trait for FieldTypes aspiring to use Image Extended plugins.
 */
trait ImageExtendedCapabilityTypeUserTrait {
    use ImageExtendedCapabilityUserTrait;
    
    protected static function gatherPropertyDefinitions(FieldStorageDefinitionInterface $field_definition, $includeParent = true) {
        $capabilityProperties = array();
        foreach(self::allCapabilityPlugins() as $capability) {
            $capabilityProperties += $capability->propertyDefinitions($field_definition);
        }
        return $capabilityProperties + ($includeParent ? parent::propertyDefinitions($field_definition) : array());
    }
    
    protected static function gatherSchema(FieldStorageDefinitionInterface $field_definition, $includeParent = true) {
        $capabilitySchemas = array();
        foreach(self::allCapabilityPlugins() as $capability) {
            $capabilitySchemas = ArrayUtils::merge($capability->schema($field_definition), $capabilitySchemas);
        }
        if($includeParent) {
            return ArrayUtils::merge($capabilitySchemas, parent::schema($field_definition));
        } else {
            return $capabilitySchemas;
        }
    }
    
    protected function invokeCapabilitiesPresave() {
        foreach(self::allCapabilityPlugins() as $capability) {
            $capability->preSave($this);
        }
    }
    
    protected function invokeCapabilitiesPostSave($update) {
        $changed = false;
        foreach(self::allCapabilityPlugins() as $capability) {
            $changed = $capability->postSave($this, $update) || $changed;
        }
        return $changed;
    }
    
    protected function invokeCapabilitiesDelete() {
        foreach(self::allCapabilityPlugins() as $capability) {
            $capability->delete($this);
        }
    }
    
    protected function invokeCapabilitiesDeleteRevision() {
        foreach(self::allCapabilityPlugins() as $capability) {
            $capability->deleteRevision($this);
        }
    }
    
    protected static function gatherDefaultFieldSettings($includeParent = true) {
        $defaultSettings = array();
        foreach(self::allCapabilityPlugins() as $plugin_id => $capability) {
            $ix = $plugin_id . '_group';
            $defaultSettings[$ix] = array(
                'capability_enabled' => 0,
            );
            $defaultSettings[$ix]['capability_settings'] = $capability->defaultFieldSettings();
        }
        if($includeParent) {
            $defaultSettings += parent::defaultFieldSettings();
        }
        
        return $defaultSettings;
    }
    
    protected function gatherFieldSettingsForm($form, FormStateInterface $form_state, $includeParent = true) {
        if($includeParent) {
            $element = parent::fieldSettingsForm($form, $form_state);
        } else {
            $element = array();
        }
        
        $settings = $this->getSettings();
        
        foreach(self::pluginManager()->getDefinitions() as $plugin_id => $pluginDef) {
            $capability = self::pluginManager()->createInstance($pluginDef['id']);
            $ix = $plugin_id . '_group';
            $element[$ix] = array(
                '#type' => 'details',
                '#open' => $settings[$ix]['capability_enabled'],
                '#title' => $pluginDef['label'],
                'capability_enabled' => array(
                    '#type' => 'checkbox',
                    '#title' => t('Enable'),
                    '#default_value' => $settings[$ix]['capability_enabled'],
                ),
            );
            $element[$ix]['capability_settings'] = $capability->fieldSettingsForm($this, $form, $form_state);
        }
        
        return $element;
    }
}