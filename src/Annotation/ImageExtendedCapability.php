<?php

/**
 * @file
 * Contains \Drupal\image_extended\Annotation\ImageExtendedCapability.
 */

namespace Drupal\image_extended\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines an image extended capability annotation object.
 *
 * Plugin Namespace: Plugin\ImageExtendedCapability
 *
 * @see plugin_api
 *
 * @Annotation
 */
class ImageExtendedCapability extends Plugin {
  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The human-readable name of the image extended capability.
   *
   * @ingroup plugin_translatable
   *
   * @var \Drupal\Core\Annotation\Translation
   */
  public $label;
}
