<?php

/**
 * @file
 * Contains \Drupal\image_extended\ExtendedCapabilityManager.
 */

namespace Drupal\image_extended;

use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;

/**
 * ImageExtendedCapability plugin manager.
 */
class ExtendedCapabilityManager extends DefaultPluginManager {
    
  /**
   * Constructs a new ExtendedCapabilityManager.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   */
    public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
        parent::__construct('Plugin/ImageExtendedCapability', $namespaces, $module_handler, 'Drupal\image_extended\ImageExtendedCapabilityInterface', 'Drupal\image_extended\Annotation\ImageExtendedCapability');
        
        $this->alterInfo('image_extended_capability_info');
        $this->setCacheBackend($cache_backend, 'image_extended_capability_plugins');
    }
}
