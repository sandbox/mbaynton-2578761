<?php
namespace Drupal\image_extended;

use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;


/**
 * Provides no-op implementations of all ImageExtendedCapability plugin methods.
 */
class ImageExtendedCapabilityBase implements ImageExtendedCapabilityInterface {
    /**
     * {@inheritdoc}
     */
    public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
        return array();
    }

    /**
     * {@inheritdoc}
     */
    public static function schema(FieldStorageDefinitionInterface $field_definition) {
        return array();
    }
    
    /**
     * {@inheritdoc}
     */
    public function preSave(FieldItemInterface $entireFieldItem) {
        
    }
    
    /**
     * {@inheritdoc}
     */    
    public function postSave(FieldItemInterface $entireFieldItem, $update) {
        return false;
    }
    
    /**
     * {@inheritdoc}
     */
    public function delete(FieldItemInterface $entireFieldItem) {
        
    }
    
    /**
     * {@inheritdoc}
     */
    public function deleteRevision(FieldItemInterface $entireFieldItem){
        
    }
    
    /**
     * {@inheritdoc}
     */
    public static function defaultFieldSettings() {
        return array();
    }
    
    /**
     * {@inheritdoc}
     */
    public function fieldSettingsForm(FieldItemInterface $entireFieldItem, array $form, FormStateInterface $form_state){
        return array('#theme' => 'image_extended_capability_no_settings');
    }    
}
