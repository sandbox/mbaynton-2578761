<?php
namespace Drupal\image_extended;

/**
 * A base trait for the _TypeUserTrait, _WidgetUserTrait, and 
 * _FormatterUserTrait traits which prescribes how to get the 
 * Image Extended Capability plugin manager. The "User" refers
 * to classes that want to *use* the capability plugins.
 * 
 * Possibly no one would ever use their own plugin manager and this is excessive 
 * indirection, but here it is. It might be useful in another
 * field type similar to Image Extended that wants to filter or
 * extend the set of available plugins.
 */
trait ImageExtendedCapabilityUserTrait {
    /**
     * Gets a plugin manager for the capabilities.
     * 
     * @return \Drupal\Component\Plugin\PluginManagerInterface
     */
    protected abstract static function pluginManager();
    
    protected static function allCapabilityPlugins() {
        $plugins = &drupal_static(__METHOD__);
        if($plugins === null) {
            foreach(self::pluginManager()->getDefinitions() as $plugin_id => $pluginDef) {
                $instantiated[$plugin_id] = self::pluginManager()->createInstance($pluginDef['id']);
            }
            $plugins = &$instantiated;
        }
        reset($plugins);
        return $plugins;
    }
}
